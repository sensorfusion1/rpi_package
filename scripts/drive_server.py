#!/usr/bin/env python

# Import required modules
import time
import RPi.GPIO as GPIO
#from __future__ import print_function
from visual.srv import *
import rospy

ang = 12 # Connected to GPIO 18, PWM0 (pin 12)
speed = 16 # Connected to GPIO 23
speed2 = 18 # Connected to GPIO 24
# 5.5 forward
def handle_drive(req):
    global duty
    print("Inputs recieved ", req.drive)

    #insert code for producing steering angle
    if req.drive == "a":
        if duty == 7:
            duty = 5.5
            pi_pwm_ang.ChangeDutyCycle(duty)
        else:
            duty = 4.5
            pi_pwm_ang.ChangeDutyCycle(duty)
    elif req.drive == "d":
        if duty == 4.5:
            duty = 5.5
            pi_pwm_ang.ChangeDutyCycle(duty)
        else:
            duty = 7
            pi_pwm_ang.ChangeDutyCycle(duty)
    elif req.drive == "w":
        pi_pwm_speed.ChangeDutyCycle(100)
        pi_pwm_speed2.ChangeDutyCycle(0)
    elif req.drive == "s":
        pi_pwm_speed.ChangeDutyCycle(0)
        pi_pwm_speed2.ChangeDutyCycle(100)
    elif req.drive == "q":
        pi_pwm_speed.ChangeDutyCycle(100)
        pi_pwm_speed2.ChangeDutyCycle(100)
    else:
        pass

    return DriveResponse(req.drive)

def drive_server():
    rospy.init_node('drive_server')
    s = rospy.Service('drive_command', Drive, handle_drive)
    print("Ready to recieve driving inputs.")
    rospy.spin()

if __name__ == "__main__":
    global duty
    # Declare the GPIO settings
    GPIO.setmode(GPIO.BOARD)
    # set up GPIO pins
    GPIO.setup(ang, GPIO.OUT) 
    GPIO.setup(speed, GPIO.OUT)
    GPIO.setup(speed2, GPIO.OUT)
    pi_pwm_ang = GPIO.PWM(ang, 50) #50 hz
    pi_pwm_speed = GPIO.PWM(speed,100) #100 hz
    pi_pwm_speed2 = GPIO.PWM(speed2,100) #100 hz
    duty = 0
    pi_pwm_ang.start(duty)
    pi_pwm_speed.start(duty)
    pi_pwm_speed2.start(duty)

    drive_server()

    GPIO.cleanup()
